#include <bits/stdc++.h>

using namespace std;

class Arcade {
public:
    vector<long long> inputs;
    vector<long long> outputs;
    vector<long long> memory;
    map<pair<int, int>, int > screen;
    long long score = 0;
    bool halted = false;

    Arcade(vector<long long> mem, int quarters, bool pauseOnOutput = false) :  pauseOnOutput(pauseOnOutput) {
        memory = vector<long long>(20000);
        for(int i = 0; i < mem.size(); i++) {
            memory[i] = mem[i];
        }
        memory[0] = quarters;
    }

    void execute() {
        runProgram();
    }
private:
    bool pauseOnOutput = false;

    int pc = 0;
    int relativeBase = 0;
    int currInput = 0;
    bool first = true;

    /**
     * Decodes an instruction by obtaining its op code and parameter modes
     * @param instr the instruction to decode
     * @return a vector where the first element is the op code, the next
     * three elements are parameter modes.
        * note: not all instructions will use the 3 parameter modes
     */
    vector<int> decode(int instr) {
        int opCode = instr % 100;
        vector<int> d({opCode, (instr / 100) % 10, (instr / 1000) % 10, (instr / 10000) % 10});
        return d;
    }

    int getAddress(int mode, int addr) {
        if(mode == 0)
            return memory[addr];
        else if(mode == 1)
            return addr;
        else
            return relativeBase + memory[addr];
    }

    void runProgram() {
        while(!halted) {
            vector<int> dec = decode(memory[pc]);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] + memory[arg2];

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] * memory[arg2];

                pc += 4;
            }
            else if(op == 3) { // in
                int arg1 = getAddress(dec[1], pc + 1);
//                memory[arg1] = inputs[currInput];

                int in = getInput();
                memory[arg1] = in;

//                currInput++;
                pc += 2;
            }
            else if(op == 4) { // out
                int arg1 = getAddress(dec[1], pc + 1);
                outputs.push_back(memory[arg1]);

                pc += 2;

                if(outputs.size() == 3) {
                    if(outputs[0] == -1 && outputs[1] == 0)
                        score = outputs[2];
                    else
                        screen[make_pair(outputs[0], outputs[1])] = outputs[2];
                    outputs.clear();
                }

                if(pauseOnOutput)
                    break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] != 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] == 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] < memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] == memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 9) { // adjust relative base
                int arg1 = getAddress(dec[1], pc + 1);
                relativeBase += memory[arg1];

                pc += 2;
            }
            else if(op == 99) {
                halted = true;
                break;
            }
        }
    }

    void printScreen() {
        int minX = INT_MAX, maxX = INT_MIN;
        int minY = INT_MAX, maxY = INT_MIN;

        for(auto e : screen) {
            minX = min(minX, e.first.first);
            maxX = max(maxX, e.first.first);

            minY = min(minY, e.first.second);
            maxY = max(maxY, e.first.second);
        }

        for(int x = minX; x <= maxX; x++) {
            for(int y = minY; y <= maxY; y++) {
                int tile = screen[make_pair(x, y)];
                char c;

                if(tile == 0) // empty
                    c = '.';
                else if(tile == 1) // wall
                    c = '[';
                else if(tile == 2) // block
                    c = '#';
                else if(tile == 3) // horizontal paddle
                    c = '|';
                else // ball
                    c = 'o';

                cout << c << " ";
            }
            cout << endl;
        }
        cout << endl;
    }

    pair<int, int> getTile(int t) {
        for(auto e : screen) {
            if(e.second == t)
                return e.first;
        }
        return make_pair(-1, -1);
    }

    int getInput() {
        pair<int, int> ball = getTile(4);
        pair<int, int> paddle = getTile(3);

        if(ball.first < paddle.first)
            return -1;
        else if(ball.first > paddle.first)
            return +1;
        else
            return 0;
    }
};

int solvePart1(vector<long long> &program) {
    Arcade a(program, 1);
    a.execute();

    int result = 0;
    for(auto e : a.screen) {
        if(e.second == 2)
            result++;
    }

    return result;
}

long long solvePart2(vector<long long> &program) {
    Arcade a(program, 2);
    a.execute();

    return a.score;
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<long long> program;

    while(getline(in, line, ',')) {
        program.push_back(stol(line));
    }

    cout << "part1: " << solvePart1(program) << endl;
    cout << "part2: " << solvePart2(program) << endl;

    in.close();
    return 0;
}