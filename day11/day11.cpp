#include <bits/stdc++.h>

using namespace std;

class IntCodeComputer {
public:
    vector<long long> inputs;
    vector<long long> outputs;
    vector<long long> memory;
    bool halted = false;

    IntCodeComputer(vector<long long> mem, bool pauseOnOutput = false) :  pauseOnOutput(pauseOnOutput) {
        memory = vector<long long>(20000);
        for(int i = 0; i < mem.size(); i++) {
            memory[i] = mem[i];
        }
    }

    void execute() {
        runProgram();
    }
private:
    bool pauseOnOutput = false;

    int pc = 0;
    int relativeBase = 0;
    int currInput = 0;
    bool first = true;

    /**
     * Decodes an instruction by obtaining its op code and parameter modes
     * @param instr the instruction to decode
     * @return a vector where the first element is the op code, the next
     * three elements are parameter modes.
        * note: not all instructions will use the 3 parameter modes
     */
    vector<int> decode(int instr) {
        int opCode = instr % 100;
        vector<int> d({opCode, (instr / 100) % 10, (instr / 1000) % 10, (instr / 10000) % 10});
        return d;
    }

    int getAddress(int mode, int addr) {
        if(mode == 0)
            return memory[addr];
        else if(mode == 1)
            return addr;
        else
            return relativeBase + memory[addr];
    }

    void runProgram() {
        while(!halted) {
//            if(memory[651] > 90000000000000 && first) {
//                cout << "inputs size: " << inputs.size() << endl;
//                cout << "pc: " << pc << endl;
//                first = false;
//            }

            vector<int> dec = decode(memory[pc]);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] + memory[arg2];

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] * memory[arg2];

                pc += 4;
            }
            else if(op == 3) { // in
                int arg1 = getAddress(dec[1], pc + 1);
                memory[arg1] = inputs[currInput];

                currInput++;
                pc += 2;
            }
            else if(op == 4) { // out
                int arg1 = getAddress(dec[1], pc + 1);
                outputs.push_back(memory[arg1]);

                pc += 2;

                if(pauseOnOutput)
                    break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] != 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] == 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] < memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] == memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 9) { // adjust relative base
                int arg1 = getAddress(dec[1], pc + 1);
                relativeBase += memory[arg1];

                pc += 2;
            }
            else if(op == 99) {
                halted = true;
                break;
            }


        }
    }
};

char changeDir(char currDir, int d) {
    if(currDir == '^' && d == 0)
        currDir = '<';
    else if(currDir == '^' && d == 1)
        currDir = '>';
    else if(currDir == '<' && d == 0)
        currDir = 'v';
    else if(currDir == '<' && d == 1)
        currDir = '^';
    else if(currDir == 'v' && d == 0)
        currDir = '>';
    else if(currDir == 'v' && d == 1)
        currDir = '<';
    else if(currDir == '>' && d == 0)
        currDir = '^';
    else if(currDir == '>' && d == 1)
        currDir = 'v';

    return currDir;
}

pair<int, int> changePos(pair<int, int> currPos, char currDir) {
    if(currDir == '^')
        currPos.second++;
    else if(currDir == '<')
        currPos.first--;
    else if(currDir == 'v')
        currPos.second--;
    else if(currDir == '>')
        currPos.first++;

    return currPos;
}

map<pair<int, int>, int> solvePart1(vector<long long> program, int startColor) {
    IntCodeComputer c(program, true);

    map<pair<int, int>, int> grid;
    pair<int, int> pos = make_pair(0, 0);
    grid[pos] = startColor;
    char dir = '^';

    while(!c.halted) {
        c.inputs.push_back(grid[pos]);

        while(c.outputs.size() < 2 && !c.halted)
            c.execute();

        int newDir = c.outputs.back();
        c.outputs.pop_back();

        int color = c.outputs.back();
        c.outputs.pop_back();

        grid[pos] = color;

        dir = changeDir(dir, newDir);
        pos = changePos(pos, dir);
    }

    return grid;
}

void print(map<pair<int, int>, int> grid) {
    int minX = INT_MAX, maxX = INT_MIN;
    int minY = INT_MAX, maxY = INT_MIN;

    for(auto e : grid) {
        minX = min(minX, e.first.first);
        maxX = max(maxX, e.first.first);

        minY = min(minY, e.first.second);
        maxY = max(maxY, e.first.second);
    }

    for(int y = maxY; y >= minY; y--) {
        for(int x = minX; x <= maxX; x++) {
            int color = grid[make_pair(x, y)];
            cout << ((color == 1) ? '#' : ' ');
        }
        cout << endl;
    }
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<long long> program;

    while(getline(in, line, ',')) {
        program.push_back(stol(line));
    }

    cout << "part1: " << solvePart1(program, 0).size() << endl;
    cout << "part2: " << endl;
    print(solvePart1(program, 1));

    in.close();
    return 0;
}