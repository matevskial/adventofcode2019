#include <bits/stdc++.h>

using namespace std;

int countDetected(int x, int y, vector<string> &theMap) {
    set<double> slopes;

    int result = 0;

    for(int i = 0; i < theMap.size(); i++) {
        for(int j = 0; j < theMap[i].length(); j++) {
            if((j == x && i == y) || theMap[i][j] == '.') continue;

            double slope = atan2(j - x, i - y);

            if(slopes.count(slope) == 0)
                result++;

            slopes.insert(slope);
        }
    }

    return result;
}

pair<pair<int, int>, int> solvePart1(vector<string> &theMap) {
    int result = 0;
    int x = 0, y = 0;
    for(int i = 0; i < theMap.size(); i++) {
        for(int j = 0; j < theMap[i].length(); j++) {
            if(theMap[i][j] == '#') {
                int count = countDetected(j, i, theMap);
                if(count > result) {
                    result = count;
                    x = j;
                    y = i;
                }
            }
        }
    }

    return make_pair(make_pair(x, y), result);
}

double dist(int x1, int y1, int x2, int y2) {
    int dx = x2 - x1;
    int dy = y2 - y1;
    return sqrt(pow(dx, 2) + pow(dy, 2));
}

int solvePart2(int x, int y,  vector<string> &theMap) {

    map<double, vector<pair<int, int> >, greater<double> > angles;

    for(int i = 0; i < theMap.size(); i++) {
        for(int j = 0; j < theMap[i].length(); j++) {
            if((j == x && i == y) || theMap[i][j] == '.') continue;

            double angle = atan2(j - x, i - y);
            angles[angle].push_back(make_pair(j, i));
        }
    }

    for(auto &a : angles) {
        sort(a.second.begin(), a.second.end(), [&, x, y](auto a, auto b) {
            return dist(x, y, a.first, a.second) > dist(x, y, b.first, b.second);
        });
    }

    int steps = 0;
    int n = 200;
    pair<int, int> theAsteroid;
    while(true) {
        for(auto &a : angles) {
            if(a.second.empty()) continue;

            steps++;
            pair<int, int> as = a.second.back();
            a.second.pop_back();

            if(steps == n) {
                theAsteroid = as;
                break;
            }
        }

        if(steps == n)
            break;
    }

    return 100*theAsteroid.first + theAsteroid.second;
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<string> theMap;

    while(getline(in, line)) {
        theMap.push_back(line);
    }

    pair<pair<int, int>, int> r = solvePart1(theMap);

    cout << "part1: " << r.second << endl;
    cout << "part2: " << solvePart2(r.first.first, r.first.second, theMap) << endl;

    in.close();
    return 0;
}