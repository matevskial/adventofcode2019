#include <bits/stdc++.h>

using namespace std;

string phase(string numbers) {
    vector<int> pattern({0, 1, 0, -1});
    string result;

    for(int p = 1; p <= numbers.length(); p++) {
        // k - index of the current pattern value to take, c - a counter to help calculating k
        // default value of c is 2 to get the effect of left-padding the final pattern
        int k = 0, c = 2;
        int res = 0;

        for(int i = 0; i < numbers.length(); i++) {
            if(c > p) {
                c = 1;
                k = (k + 1) % 4;
            }

            int d = numbers[i] - '0';
            int r = d*pattern[k];
            res += r;
            c++;

//            cout << pattern[k] << " ";
        }
//        cout << endl;
        res = abs(res) % 10;
        result.push_back(res + '0');
    }

    return result;
}

string solvePart1(string numbers) {
    for(int i = 1; i <= 100; i++) {
        numbers = phase(numbers);
    }
    return numbers.substr(0, 8);
}

void printPatterns() {
    vector<int> pattern({0, 1, 0, -1});
    string numbers = "456310967512343";
    cout << setw(3) << "x";
    for(int i = 0; i < numbers.size(); i++) {
        cout << setw(3) << numbers[i];
    }
    cout << endl;
    for(int p = 1; p <= numbers.size(); p++) {
        int k = 0, c = 2;
        int res = 0;
        cout << setw(3) << p;
        for(int i = 0; i < numbers.size(); i++) {
            if(c > p) {
                c = 1;
                k = (k + 1) % 4;
            }


            cout << setw(3) << pattern[k];
            int d = numbers[i] - '0';
            int r = d*pattern[k];
            res += r;
            c++;
        }
        cout << setw(3) << abs(res) % 10 << endl;
    }
    cout << endl;
}

string phasePart2(string numbers, int startSecondHalf) {
    string result = "";
    int sum = 0;
    for(int i = 0; i < numbers.size(); i++) {
        if(i < startSecondHalf) {
            result.push_back(numbers[i]);
        } else {
            sum += (numbers[i] - '0');
        }
    }

    for(int i = startSecondHalf; i < numbers.size(); i++) {
        if(i > startSecondHalf)
            sum -= (numbers[i - 1] - '0');

        int res = sum % 10;
        result.push_back(res + '0');
    }

    return result;
}

// using the function printPattern
// you can notice that it is easy to calculate the phase of the second half of the input in linear time
// instead of quadratic time that the function phasePart1 calculates
// plus, the offset of the location of the eight-digit message is larger than the size of half the input
string solvePart2(string numbers) {
    string newNumbers = "";
    for(int i = 1; i <= 10000; i++) {
        newNumbers.append(numbers);
    }

    int finalSize = newNumbers.size();
    int secondHalfStart = finalSize / 2;
    int offset = stoi(newNumbers.substr(0, 7));

    for(int i = 1; i <= 100; i++) {
        newNumbers = phasePart2(newNumbers, secondHalfStart);
    }

    return newNumbers.substr(offset, 8);
}

int main() {
    ifstream in("input.txt");
    string numbers;
    getline(in, numbers);

    cout << "part1: " << solvePart1(numbers) << endl;
    cout << "part2: " << solvePart2(numbers) << endl;

    in.close();
    return 0;
}