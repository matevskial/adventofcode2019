#include <bits/stdc++.h>

using namespace std;

vector<string> split(string s, string delimiter) {
    vector<string> result;

    int pos = 0;
    string token;
    while ((pos = s.find(delimiter)) != string::npos) {
        token = s.substr(0, pos);
        result.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    result.push_back(s);

    return result;
}

vector<int> parseCoord(string s) {
    s.pop_back();
    vector<string> coords = split(s.substr(1), ", ");

    vector<int> result;
    for(string &c : coords) {
        vector<string> p = split(c, "=");
        result.push_back(stoi(p[1]));
    }

    return result;
}

void applyGravity(vector<vector<int> > &moons, vector<vector<int> > &velocities) {
    for(int i = 0; i < moons.size(); i++) {
        for(int j = i + 1; j < moons.size(); j++) {
            for(int c = 0; c < 3; c++) {
                int changeI = (moons[i][c] < moons[j][c]) ? +1 :
                              (moons[i][c] > moons[j][c]) ? -1 : 0;
                int changeJ = (moons[j][c] < moons[i][c]) ? +1 :
                              (moons[j][c] > moons[i][c]) ? -1 : 0;

                velocities[i][c] += changeI;
                velocities[j][c] += changeJ;
            }
        }
    }
}

void applyVelocity(vector<vector<int> > &moons, vector<vector<int> > &velocities) {
    for(int i = 0; i < moons.size(); i++) {
        for(int c = 0; c < 3; c++) {
            moons[i][c] += velocities[i][c];
        }
    }
}

int getEnergyAfterSimulation(vector<vector<int> > moons, int n) {
    vector<vector<int> > velocities(moons.size(), vector<int>({0, 0, 0}));

    for(int s = 0; s < n; s++) {
        applyGravity(moons, velocities);
        applyVelocity(moons, velocities);
    }

    int result = 0;
    for(int i = 0; i < moons.size(); i++) {
        int pot = 0;
        int kin = 0;

        for(int c = 0; c < 3; c++) {
            pot += abs(moons[i][c]);
            kin += abs(velocities[i][c]);
        }

        result += (pot*kin);
    }

    return result;
}

string getState(vector<vector<int> > &moons, vector<vector<int> > &velocities) {
    string res;
    for(int i = 0; i < moons.size(); i++) {
        for(int c = 0; c < 3; c++) {
            res.append(to_string(moons[i][c]));
            res.append(to_string(velocities[i][c]));
        }
    }

    return res;
}

void printCoord(vector<int> coord) {
    cout << "<x=" << setw(3) << coord[0] << ", y=" << setw(3) << coord[1]
    << ", z=" << setw(3) << coord[2] << ">";
}

void print(vector<vector<int> > &moons, vector<vector<int> > &velocities) {
    for(int i = 0; i < moons.size(); i++) {
        printCoord(moons[i]);
        cout << ", ";
        printCoord(velocities[i]);
        cout << endl;
    }
}

bool allZeros(vector<vector<int> > &velocities) {
    for(int i = 0; i < velocities.size(); i++) {
        for(int c = 0; c < 3; c++) {
            if(velocities[i][c] != 0)
                return false;
        }
    }

    return true;
}

int simulate(vector<vector<int> > moons) {
    vector<vector<int> > velocities(moons.size(), vector<int>({0, 0, 0}));
    unordered_set<string> seen;
    seen.insert(getState(moons, velocities));
    for(long long s = 0; s < 2773 ; s++) {
        cout << "After " << s << " steps:" << endl;
        print(moons, velocities);
        cout << endl;

        applyGravity(moons, velocities);
        applyVelocity(moons, velocities);
    }

    return -1;
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<vector<int> > moons;

    while(getline(in, line)) {
        moons.push_back(parseCoord(line));
    }

    cout << "part1: " << getEnergyAfterSimulation(moons, 1000) << endl;
//    cout << "part2: " << simulate(moons);

    in.close();
    return 0;
}