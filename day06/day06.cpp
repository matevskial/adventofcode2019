#include <bits/stdc++.h>

using namespace std;

int solve(map<string, vector<string> > &graph, string s, string e, bool part2) {
    map<string, int> dists;
    dists[s] = 0;
    queue<string> q;
    q.push(s);

    while(!q.empty()) {
        string o = q.front();
        q.pop();

        for(string &orbits : graph[o]) {
            if(dists.find(orbits) != dists.end())
                continue;

            dists[orbits] = dists[o] + 1;
            q.push(orbits);
        }
    }

    int resultPart1 = 0;
    for(auto o : dists) {
        resultPart1 += o.second;
    }

    int resultPart2 = dists[e] - 2;

    return (part2) ? resultPart2 : resultPart1;
}

int main() {
    ifstream in("input.txt");
    string line;
    map<string, vector<string> > graph;
    map<string, vector<string> > graphPart2;
    while(getline(in, line)) {
        stringstream ss(line);

        string obj;
        getline(ss, obj, ')');
        string orbits;
        getline(ss, orbits);

        graph[obj].push_back(orbits);

        graphPart2[obj].push_back(orbits);
        graphPart2[orbits].push_back(obj);
    }

    cout << "part1: " << solve(graph, "COM", "", false) << endl;
    cout << "part2: " << solve(graphPart2, "YOU", "SAN", true) << endl;

    in.close();
    return 0;
}