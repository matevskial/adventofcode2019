#include <bits/stdc++.h>

using namespace std;

class IntCodeComputer {
public:
    vector<long long> inputs;
    vector<long long> outputs;
    bool halted = false;

    IntCodeComputer(vector<long long> memory, bool pauseOnOutput = false) : memory(memory), pauseOnOutput(pauseOnOutput) {
        memory.resize((int)memory.size() + 10000);
    }

    void execute() {
        runProgram();
    }
private:
    bool pauseOnOutput = false;
    vector<long long> memory;
    int pc = 0;
    int relativeBase = 0;
    int currInput = 0;

    /**
     * Decodes an instruction by obtaining its op code and parameter modes
     * @param instr the instruction to decode
     * @return a vector where the first element is the op code, the next
     * three elements are parameter modes.
        * note: not all instructions will use the 3 parameter modes
     */
    vector<int> decode(int instr) {
        int opCode = instr % 100;
        vector<int> d({opCode, (instr / 100) % 10, (instr / 1000) % 10, (instr / 10000) % 10});
        return d;
    }

    int getAddress(int mode, int addr) {
        if(mode == 0)
            return memory[addr];
        else if(mode == 1)
            return addr;
        else
            return relativeBase + memory[addr];
    }

    void runProgram() {
        while(!halted) {
            vector<int> dec = decode(memory[pc]);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] + memory[arg2];

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] * memory[arg2];

                pc += 4;
            }
            else if(op == 3) { // in
                int arg1 = getAddress(dec[1], pc + 1);
                memory[arg1] = inputs[currInput];

                currInput++;
                pc += 2;
            }
            else if(op == 4) { // out
                int arg1 = getAddress(dec[1], pc + 1);
                outputs.push_back(memory[arg1]);

                pc += 2;

                if(pauseOnOutput)
                    break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] != 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] == 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] < memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] == memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 9) { // adjust relative base
                int arg1 = getAddress(dec[1], pc + 1);
                relativeBase += memory[arg1];

                pc += 2;
            }
            else if(op == 99) {
                halted = true;
                break;
            }
        }
    }
};

long long solveBothParts(vector<long long> &program, int input) {
    IntCodeComputer c(program);
    c.inputs.push_back(input);
    c.execute();

    return c.outputs.back();
}

int main() {
    ifstream in("input.txt");
    string line;
    getline(in, line);

    stringstream ss(line);
    vector<long long> program;
    while(getline(ss, line, ',')) {
        program.push_back(stol(line));
    }

    cout << "part1: " << solveBothParts(program, 1) << endl;
    cout << "part2: " << solveBothParts(program, 2) << endl;

    in.close();
    return 0;
}