#include <bits/stdc++.h>

using namespace std;

class IntCodeComputer {
public:
    vector<long long> inputs;
    vector<long long> outputs;
    vector<long long> memory;
    bool halted = false;
    int pauseOnCount =1;

    IntCodeComputer(vector<long long> mem, bool pauseOnOutput = false, int pauseOnCount = 1) :  pauseOnOutput(pauseOnOutput) {
        memory = vector<long long>(20000);
        for(int i = 0; i < mem.size(); i++) {
            memory[i] = mem[i];
        }
        this->pauseOnCount = pauseOnCount;
    }

    void execute() {
        runProgram();
    }
private:
    bool pauseOnOutput = false;

    int pc = 0;
    int relativeBase = 0;
    int currInput = 0;
    bool first = true;

    /**
     * Decodes an instruction by obtaining its op code and parameter modes
     * @param instr the instruction to decode
     * @return a vector where the first element is the op code, the next
     * three elements are parameter modes.
        * note: not all instructions will use the 3 parameter modes
     */
    vector<int> decode(int instr) {
        int opCode = instr % 100;
        vector<int> d({opCode, (instr / 100) % 10, (instr / 1000) % 10, (instr / 10000) % 10});
        return d;
    }

    int getAddress(int mode, int addr) {
        if(mode == 0)
            return memory[addr];
        else if(mode == 1)
            return addr;
        else
            return relativeBase + memory[addr];
    }

    void runProgram() {
        while(!halted) {
            vector<int> dec = decode(memory[pc]);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] + memory[arg2];

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] * memory[arg2];

                pc += 4;
            }
            else if(op == 3) { // in
                int arg1 = getAddress(dec[1], pc + 1);
                memory[arg1] = inputs[currInput];

                currInput++;
                pc += 2;
            }
            else if(op == 4) { // out
                int arg1 = getAddress(dec[1], pc + 1);
                outputs.push_back(memory[arg1]);

                pc += 2;

                if(pauseOnOutput && outputs.size() == pauseOnCount)
                    break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] != 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] == 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] < memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] == memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 9) { // adjust relative base
                int arg1 = getAddress(dec[1], pc + 1);
                relativeBase += memory[arg1];

                pc += 2;
            }
            else if(op == 99) {
                halted = true;
                break;
            }

        }
    }
};

int dirRow[] = {-1, 1, 0, 0};
int dirCol[] = {0, 0, 1, -1};

void printArea(map<pair<int, int>, int> &area) {
    int minX = INT_MAX, maxX = INT_MIN;
    int minY = INT_MAX, maxY = INT_MIN;

    for(auto e : area) {
        minX = min(minX, e.first.first);
        maxX = max(maxX, e.first.first);

        minY = min(minY, e.first.second);
        maxY = max(maxY, e.first.second);
    }

    for(int x = minX; x <= maxX; x++) {
        for(int y = minY; y <= maxY; y++) {
            int tile = area[make_pair(x, y)];
            char c;

            if(tile == 0) // wall
                c = '#';
            else if(tile == 1) // empty
                c = '.';
            else // oxygen
                c = 'o';

            cout << c << " ";
        }
        cout << endl;
    }
    cout << endl;
}

int fewestMovements(map<pair<int, int>, int> &grid, pair<int, int> start) {
    map<pair<int, int>, int > dist;
    queue<pair<int, int> > q;

    dist[start] = 0;
    q.push(start);

    while(!q.empty()) {
        pair<int, int> pos = q.front();
        q.pop();
        int d = dist[pos];

        for(int i = 0; i < 4; i++) {
            pair<int, int> newPos = {pos.first + dirRow[i], pos.second + dirCol[i]};

            if(dist.count(newPos) > 0 || grid[newPos] == 0) continue;

            int newDist = d + 1;
            dist[newPos] = newDist;

            if(grid[newPos] == 2)
                return newDist;

            q.push(newPos);
        }
    }

    return -1;
}

int makeMove(IntCodeComputer &c, int direction) {
    c.inputs.push_back(direction);
    c.execute();
    int response =  c.outputs.back();
    c.outputs.clear();

    return response;
}

int opositeDir(int direction) {
    return direction + (direction % 2 == 0 ? -1 : 1);
}

pair<int, int> changePos(pair<int, int> pos, int direction) {
    return {pos.first + dirRow[direction - 1], pos.second + dirCol[direction - 1]};
}

// uses dfs traversal to explore the area
int generateArea(IntCodeComputer &c, map<pair<int, int>, int> &area, pair<int, int> pos) {
    for(int i = 0; i < 4; i++) {
        pair<int, int> newPos = changePos(pos, i + 1);
        if(area.find(newPos) != area.end())
            continue;

        int response = makeMove(c, i + 1);
        area[newPos] = response;

        if(response == 1) {
            generateArea(c, area, newPos);
        }

        if(response != 0)
            makeMove(c, opositeDir(i + 1));
    }

    return -1;
}

int solvePart1(vector<long long> &program, map<pair<int, int>, int> &area) {
    IntCodeComputer c(program,  true);
    area[{0, 0}] = 1;
    generateArea(c, area, {0,0});
    printArea(area);
    int part1 = fewestMovements(area, {0,0});
    return part1;
}

int solvePart2(map<pair<int, int>, int> &area) {
    // find the location of oxygen
    pair<int, int> oxygenPos;
    for(auto e : area) {
        if(e.second == 2) {
            oxygenPos = e.first;
            break;
        }
    }

    int minutes = 0;

    vector<pair<int, int>> q;
    q.push_back(oxygenPos);

    int size = 1;

    while(q.size() != 0) {

        int newSize = 0;
        vector<pair<int, int>> newQ;
        for(auto p : q) {
            for(int i = 0; i < 4; i++) {
                pair<int, int> next = {p.first + dirRow[i], p.second + dirCol[i]};
                if(area[next] == 1) {
                    area[next] = 2;
                    newQ.push_back(next);
                    newSize++;
                }
            }
        }

        size = newSize;
        q = newQ;

        if(q.size() != 0)
            minutes++;
    }

    return minutes;
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<long long> program;

    while(getline(in, line, ',')) {
        program.push_back(stol(line));
    }

    map<pair<int, int>, int> area;
    int part1 = solvePart1(program, area);
    int part2 = solvePart2(area);

    cout << "part1: " << part1 << endl;
    cout << "part2: " << part2 << endl;

    in.close();
    return 0;
}