#include <bits/stdc++.h>

using namespace std;

int runProgram(vector<int> program, int noun, int verb) {
    program[1] = noun;
    program[2] = verb;

    for(int i = 0; i < program.size(); i+=4) {
        int op = program[i];
        int arg1 = program[i+1], arg2 = program[i+2], out = program[i+3];
        if(op == 1) {
           program[out] = program[arg1] + program[arg2];
        }
        else if(op == 2) {
           program[out] = program[arg1] * program[arg2];
        }
        else {
           break;
        }
    }

    return program[0];
}

int solvePart2(vector<int> program, int val) {
    for(int i = 0; i < 100; i++) {
        for(int j = 0; j < 100; j++) {
            if(runProgram(program, i, j) == val) {
                return 100*i + j;
            }
        }
    }
    return -1;
}

int main() {
    fstream in("input.txt");
    string num;
    vector<int> program;
    while(getline(in, num, ',')) {
        program.push_back(stoi(num));
    }

    int part1 = runProgram(program, 12, 2);
    int part2 = solvePart2(program, 19690720);
    cout << "part1: " << part1 << endl;
    cout << "part2: " << part2;

    in.close();

    return 0;
}