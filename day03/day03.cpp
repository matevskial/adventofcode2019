#include <bits/stdc++.h>

using namespace std;

int dist(pair<int,int> p) {
    return abs(p.first) + abs(p.second);
}

vector<pair<int, int> > getIntersections(map<pair<int, int>, int> &path1, map<pair<int, int>, int> &path2) {
    vector<pair<int, int> > result;
    for(auto p : path2) {
        if(path1.find(p.first) != path1.end())
            result.push_back(p.first);
    }
    return result;
}

map<pair<int, int>, int> generatePath(string &wire) {
    map<pair<int, int>, int> result;

    stringstream ss(wire);
    string dir;

    pair<int, int> curr = make_pair(0, 0);
    int steps = 0;
    while(getline(ss, dir, ',')) {
        char d = dir[0];
        int s = stoi(dir.substr(1));

        if(d == 'L') {
            for(int xi = curr.first - 1; xi >= curr.first - s; xi--) {
                steps++;
                pair<int, int> p = make_pair(xi, curr.second);
                if(result.find(p) == result.end()) {
                    result[p] = steps;
                }
            }
            curr.first -= s;
        }
        else if(d == 'R') {
            for(int xi = curr.first + 1; xi <= curr.first + s; xi++) {
                steps++;
                pair<int, int> p = make_pair(xi, curr.second);
                if(result.find(p) == result.end()) {
                    result[p] = steps;
                }
            }
            curr.first += s;
        }
        else if(d == 'D') {
            for(int yi = curr.second - 1; yi >= curr.second - s; yi--) {
                steps++;
                pair<int, int> p = make_pair(curr.first, yi);
                if(result.find(p) == result.end()) {
                    result[p] = steps;
                }
            }
            curr.second -= s;
        }
        else if(d == 'U') {
            for(int yi = curr.second + 1; yi <= curr.second + s; yi++) {
                steps++;
                pair<int, int> p = make_pair(curr.first, yi);
                if(result.find(p) == result.end()) {
                    result[p] = steps;
                }
            }
            curr.second += s;
        }
    }

    return result;
}

int solvePart1(vector<pair<int, int> > &points) {
    int result = INT_MAX;
    for(auto p : points) {
        result = min(result, dist(p));
    }

    return result;
}

int solvePart2(map<pair<int, int>, int> &path1, map<pair<int, int>, int> &path2, vector<pair<int, int> > &points) {
    int result = INT_MAX;

    for(auto p : points) {
        int steps = path1[p] + path2[p];
        result = min(result, steps);
    }

    return result;
}

int main() {
    ifstream in("input.txt");
    string line;

    getline(in, line);
    map<pair<int, int>, int> path1 = generatePath(line);

    getline(in, line);
    map<pair<int, int>, int> path2 = generatePath(line);

    vector<pair<int, int> > intersections = getIntersections(path1, path2);

    cout << "part1: " << solvePart1(intersections) << endl;
    cout << "part2: " << solvePart2(path1, path2, intersections);

    in.close();

    return 0;
}