#include <bits/stdc++.h>

using namespace std;

int count(string &s, char c) {
    return count(s.begin(), s.end(), c);
}

vector<string> getLayers(string &s, int w, int h) {
    vector<string> result;
    int sz = w * h;

    for(int i = 0; i < s.length(); i += sz) {
        result.push_back(s.substr(i, sz));
    }

    return result;
}

int solvePart1(vector<string> &layers) {
    int minCountZeros = INT_MAX;
    int result = 0;

    for(string &layer : layers) {
        int cntZeros = count(layer, '0');
        if(cntZeros < minCountZeros) {
            minCountZeros = cntZeros;
            result = count(layer, '1') * count(layer, '2');
        }
    }

    return result;
}

string solvePart2(vector<string> &layers, int w, int h) {
    string result;
    int sz = w * h;

    for(int i = 0; i < sz; i++) {
        char r = '2';
        for(string &layer : layers) {
            if(layer[i] != '2') {
                r = layer[i];
                break;
            }
        }

        result.push_back(r);
    }

    return result;
}

string print(string s, int w, int h) {
    int sz = w * h;

    for(int r = 0; r < h; r++) {
        for(int c = 25*r; c < 25*(r+1); c++) {
            cout << ((s[c] == '0') ? ' ' : '#');
        }
        cout << endl;
    }
}

int main() {
    ifstream in("input.txt");
    string line;
    getline(in, line);

    int w = 25, h = 6;

    vector<string> layers = getLayers(line, w, h);

    cout << "part1: " << solvePart1(layers) << endl;
    cout << "part2: " << endl;
    print(solvePart2(layers, w, h), w, h);

    in.close();
    return 0;
}