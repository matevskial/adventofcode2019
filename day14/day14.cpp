#include <bits/stdc++.h>
#include <utility>

using namespace std;

struct Reaction {
    int cnt;
    vector<pair<int, string> > chems;
};

pair<int, string> getChem(string s) {
    pair<int, string> res;
    stringstream ss(s);

    getline(ss, s, ' ');
    res.first = stoi(s);

    getline(ss, s, ' ');
    res.second = s;

    return res;
}

map<string, long long> built;

void build(string toBuild, Reaction &r, long long quantity, map<string, Reaction> &reactions) {
    if(toBuild == "ORE") { // collect needed ORE
        built["ORE"] += quantity;
        return;
    }

    if(quantity <= 0) // no need to build
        return;

    int reactionCnt = r.cnt;
    long long repetitions = (quantity / reactionCnt) + ((quantity % reactionCnt != 0) ? 1 : 0);
    for(const auto& element : r.chems) {
        string chem = element.second;
        int cnt = element.first;

        long long chemQuantity = repetitions * cnt;
        if(chem == "ORE") {
            build(chem, reactions[chem], chemQuantity, reactions);
        } else {
            long long toBuildQuantity = chemQuantity - built[chem];
            build(chem, reactions[chem], toBuildQuantity, reactions);
        }

        if(chem != "ORE") { // only use other elements, ORE quantity is just accumulated
            built[chem] -= chemQuantity;
        }
    }

    built[toBuild] += reactionCnt * repetitions;
}

long long solvePart1(map<string, Reaction> &reactions, long long quantity) {
    build( "FUEL", reactions["FUEL"], quantity, reactions);
    return built["ORE"];
}

// solution by binary searching the answer
// basically with binary search we find the maximum number of fuel that is created using one trillion or less ORES
long long solvePart2(map<string, Reaction> &reactions) {
    long long one_trillion = 1000000000000;

    long long left = 1, right = one_trillion;
    long long result = 1;
    while(left <= right) {
        long long mid = (right - left) / 2 + left;
        built.clear();
        solvePart1(reactions, mid);

        long long usedOres = built["ORE"];

        if(usedOres <= one_trillion) {
            result = mid;
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    return result;
}

int main() {
    ifstream in("input.txt");
    string line;
    map<string, Reaction> reactions;
    regex r(R"((\d+)(\s)(\w+))");

    while(getline(in, line)) {
        auto s = sregex_iterator(line.begin(), line.end(), r);
        auto e = sregex_iterator();

        vector<pair<int, string> > chems;

        for(auto i = s; i != e; i++) {
            smatch m = *i;
            chems.push_back(getChem(m.str()));
        }

        pair<int, string> last = chems.back();
        chems.pop_back();

        Reaction react;
        react.cnt = last.first;
        react.chems = chems;
        reactions[last.second] = react;
    }

    built.clear();

    long long part1 = solvePart1(reactions, 1);
    long long part2 = solvePart2(reactions);

    cout << "part1: " << part1 << endl;
    cout << "part2: " << part2 << endl;

    in.close();
    return 0;
}