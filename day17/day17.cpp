#include <bits/stdc++.h>

using namespace std;

class IntCodeComputer {
public:
    vector<long long> inputs;
    vector<long long> outputs;
    vector<long long> memory;
    bool halted = false;

    IntCodeComputer(vector<long long> mem, bool pauseOnOutput = false, bool interactiveInput = false) :  pauseOnOutput(pauseOnOutput), interactiveInput(interactiveInput) {
        memory = vector<long long>(80000);
        for(int i = 0; i < mem.size(); i++) {
            memory[i] = mem[i];
        }
    }

    void execute() {
        runProgram();
    }
private:
    bool pauseOnOutput = false;
    bool interactiveInput = false;

    int pc = 0;
    int relativeBase = 0;
    int currInput = 0;
    bool first = true;

    /**
     * Decodes an instruction by obtaining its op code and parameter modes
     * @param instr the instruction to decode
     * @return a vector where the first element is the op code, the next
     * three elements are parameter modes.
        * note: not all instructions will use the 3 parameter modes
     */
    vector<int> decode(int instr) {
        int opCode = instr % 100;
        vector<int> d({opCode, (instr / 100) % 10, (instr / 1000) % 10, (instr / 10000) % 10});
        return d;
    }

    int getAddress(int mode, int addr) {
        if(mode == 0)
            return memory[addr];
        else if(mode == 1)
            return addr;
        else
            return relativeBase + memory[addr];
    }

    void runProgram() {
        while(!halted) {
            vector<int> dec = decode(memory[pc]);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] + memory[arg2];

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                memory[out] = memory[arg1] * memory[arg2];

                pc += 4;
            }
            else if(op == 3) { // in
                int arg1 = getAddress(dec[1], pc + 1);
                char in;
                if(interactiveInput) {
                    cout << "input: ";
                    cin >> in;
                } else {
                    in = inputs[currInput];
                    currInput++;
                }

                memory[arg1] = in;

                pc += 2;
            }
            else if(op == 4) { // out
                int arg1 = getAddress(dec[1], pc + 1);
                outputs.push_back(memory[arg1]);

                pc += 2;

                if(pauseOnOutput)
                    break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] != 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);

                if(memory[arg1] == 0)
                    pc = memory[arg2];
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] < memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = getAddress(dec[1], pc + 1);
                int arg2 = getAddress(dec[2], pc + 2);
                int out = getAddress(dec[3], pc + 3);

                long long result = 0;
                if(memory[arg1] == memory[arg2])
                    result = 1;

                memory[out] = result;

                pc += 4;
            }
            else if(op == 9) { // adjust relative base
                int arg1 = getAddress(dec[1], pc + 1);
                relativeBase += memory[arg1];

                pc += 2;
            }
            else if(op == 99) {
                halted = true;
                break;
            }


        }
    }
};

// down, left, up, right
int dirRow[] = {1, 0, -1, 0};
int dirCol[] = {0, -1, 0, 1};

bool validPos(pair<int, int> pos, vector<vector<char> > &s) {
    return pos.first >= 0 && pos.first < s.size()
    && pos.second >= 0 && pos.second < s[0].size();
}

bool intersection(pair<int, int> pos, vector<vector<char> > &s) {
    for(int i = 0; i < 4; i++) {
        pair<int, int> newPos = {pos.first + dirRow[i], pos.second + dirCol[i]};
        if( !validPos(newPos, s) || s[newPos.first][newPos.second] != '#' )
            return false;
    }

    return true;
}

void printGrid(vector<vector<char> > &grid) {
    for(auto &row : grid) {
        for(char c : row) {
            cout << c;
        }
        cout << endl;
    }
    cout << endl;
}

vector<vector<char> > getScaffold(const vector<long long> &outputs) {
    vector<vector<char> > scaffold(1);
    for(int i = 0; i < outputs.size(); i++) {
        if(outputs[i] < 0 or outputs[i] > 255)
            continue;

        char chr = (char)outputs[i];

        if(chr == 10)
            scaffold.push_back(vector<char>());
        else
            scaffold.back().push_back(chr);
    }

    while(scaffold.back().size() == 0) scaffold.pop_back();

    return scaffold;
}

int solvePart1(vector<long long> program) {
    IntCodeComputer c(program);
    c.execute();
    vector<vector<char> > scaffold = getScaffold(c.outputs);

    int result = 0;
    for(int i = 0; i < scaffold.size(); i++) {
        for(int j = 0; j < scaffold[i].size(); j++) {
            if(scaffold[i][j] == '#' && intersection({i, j}, scaffold))
                result += (i * j);
        }
    }

    return result;
}

pair<int, int> move(pair<int, int> pos, int dir) {
    pos = {pos.first + dirRow[dir], pos.second + dirCol[dir]};

    return pos;
}

int nextDir(pair<int, int> pos, vector<vector<char> > &scaffold, int prevDir) {
    int oppositeDir = (prevDir + 2) % 4;
    for(int i = 0; i < 4; i++) {
        pair<int, int> nextPos = {pos.first + dirRow[i], pos.second + dirCol[i]};

        if(validPos(nextPos, scaffold) && scaffold[nextPos.first][nextPos.second] == '#' && i != oppositeDir)
            return i;
    }

    return -1;
}

string findAndReplaceAll(string data, const string& toSearch, const string& replaceStr) {
    size_t pos = data.find(toSearch);

    while( pos != string::npos) {
        data.replace(pos, toSearch.size(), replaceStr);

        pos = data.find(toSearch, pos + replaceStr.size());
    }
    return data;
}

// class  that maintains common state between recursive calls in the algorithm
// that solves part 2
class State {
public:
    vector<string> mfun = vector<string>(3);
    bool found = false;
    string result = "";

    string getFun(int fun) {
        if(fun == 0)
            return "A";
        else if(fun == 1)
            return "B";
        else
            return "C";
    }

    bool valid(string &str) {
        // should contain only tokens of A,B,C
        stringstream ss(str);
        string token;
        while(getline(ss, token, ',')) {
            if(token != "A" and token != "B" and token != "C")
                return false;
        }

        return str.size() <= 20;
    }
};

void calculateInputRec(string &dirs, int fun, State &s) {
    stringstream ss(dirs);
    string token;
    string dir;
    int c = 0;
    bool skipped = false;

    while(getline(ss, token, ',')) {
        if(s.found)
            break;

        c++;
        dir.append(token);

        if(dir == "A" or dir == "B" or dir == "C") {
            if(skipped)
                break;
            else {
                c = 0;
                dir = "";
                continue;
            }
        }

        skipped = true;

        if(c != 2) {
            dir.push_back(',');
            continue;
        }

        if(!s.mfun[fun].empty())
            s.mfun[fun].push_back(',');
        s.mfun[fun].append(dir);

        if(s.mfun[fun].size() > 20)
            break;

        string newDirs = findAndReplaceAll(dirs, s.mfun[fun], s.getFun(fun));

        if(fun != 2) {
            s.mfun[fun + 1].clear();
            calculateInputRec(newDirs, fun + 1, s);
        } else {
            if(s.valid(newDirs)) {
                s.found = true;
                s.result = newDirs;
                break;
            }
        }

        c = 0;
        dir = "";
    }

}

// should calculate the input automatically
string calculateInput(string &dirs) {
    State s;
    calculateInputRec(dirs, 0, s);

    string res;
    res.append(s.result); res.push_back('\n');
    res.append(s.mfun[0]); res.push_back('\n');
    res.append(s.mfun[1]); res.push_back('\n');
    res.append(s.mfun[2]); res.push_back('\n');
    res.append("n\n");

    return res;
}

long long solvePart2(vector<long long> program) {
    IntCodeComputer c(program);
    c.execute();
    vector<vector<char> > scaffold = getScaffold(c.outputs);

    pair<int, int> pos;
    for(int i = 0; i < scaffold.size(); i++) {
        for(int j = 0; j < scaffold[i].size(); j++) {
            if(scaffold[i][j] == '^') {
                pos = {i, j};
                break;
            }
        }
    }

    vector<string> dirs;
    string dirsStr;
    int prevDir = 2;
    int currDir = nextDir(pos, scaffold, 2);
    set<string> uniquePaths;
    while(currDir != -1) {
        int count = 0;

        pair<int, int> nextPos = move(pos, currDir);
        while(validPos(nextPos, scaffold) && scaffold[nextPos.first][nextPos.second] == '#') {
            pos = nextPos;
            nextPos = move(pos, currDir);
            count++;
        }

        bool isLeft;
        if(prevDir == 0)
            isLeft = (currDir == 3);
        else if(prevDir == 3)
            isLeft = (currDir == 2);
        else
            isLeft = (currDir < prevDir);

        string s = string(1, (isLeft) ? 'L' : 'R') + "," + to_string(count);

        if(dirsStr.size() != 0)
            dirsStr.push_back(',');
        dirsStr.append(s);

        dirs.push_back(s);
        uniquePaths.insert(s);

        prevDir = currDir;
        currDir = nextDir(pos, scaffold, currDir);
    }

    string in = calculateInput(dirsStr);
    program[0] = 2;
    c = IntCodeComputer(program, false, false);
    for(char chr : in) {
        c.inputs.push_back(chr);
    }
    c.execute();
    return c.outputs.back();
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<long long> program;

    while(getline(in, line, ',')) {
        program.push_back(stol(line));
    }

    cout << "part1: " << solvePart1(program) << endl;
    cout << "part2: " << solvePart2(program) << endl;

    in.close();
    return 0;
}