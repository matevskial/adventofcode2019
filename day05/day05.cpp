#include <bits/stdc++.h>

using namespace std;

int getIndex(int p, int offset, int len) {
    return min(len - 1, p + offset);
}

/* returns vector where:
 * first element is opcode
 * next element is VALUE of parameter 1
 * next element is VALUE of parameter 2 if any
 * final element is output address
 */
vector<int> decode(string instruction, int pc, vector<int> &program) {
    int len = instruction.length();
    int progLen = program.size();

    instruction = string(4 - len, '0') + instruction;
    len = instruction.length();

    bool imediateArg1 = (instruction.substr(len - 3, 1) != "0");
    bool imediateArg2 = (instruction.substr(len - 4, 1) != "0");
    vector<int> d(4);

    int off1 = getIndex(pc, 1, progLen);
    int off2 = getIndex(pc, 2, progLen);
    int off3 = getIndex(pc, 3, progLen);

    d[0] = stoi(instruction.substr(len - 2));

    if(d[0] == 1 || d[0] == 2 || d[0] == 7 || d[0] == 8) {
        d[1] = (imediateArg1) ? program[off1] : program[program[off1]];
        d[2] = (imediateArg2) ? program[off2] : program[program[off2]];
        d[3] = program[off3];
    }
    else if(d[0] == 3 || d[0] == 4) {
        d[1] = program[off1];
    }
    else if(d[0] == 5 || d[0] == 6) {
        d[1] = (imediateArg1) ? program[off1] : program[program[off1]];
        d[2] = (imediateArg2) ? program[off2] : program[program[off2]];
    }

    return d;
}

vector<int> runProgram(vector<int> program, int input) {
    int pc = 0;
    vector<int> outputs;

    while(pc < program.size()) {
        vector<int> dec = decode(to_string(program[pc]), pc, program);
        int op = dec[0];

        if(op == 1) { // add
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = arg1 + arg2;

            pc += 4;
        }
        else if(op == 2) { // mul
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = arg1 * arg2;

            pc += 4;
        }
        else if(op == 3) { // in
            int in = dec[1];
            program[in] = input;

            pc += 2;
        }
        else if(op == 4) { // out
            int out = dec[1];
            outputs.push_back(program[out]);

            pc += 2;
        }
        else if(op == 5) { // jump-if-true
            int arg1 = dec[1];
            int arg2 = dec[2];

            if(arg1 != 0)
                pc = arg2;
            else
                pc += 3;
        }
        else if(op == 6) { // jump-if-false
            int arg1 = dec[1];
            int arg2 = dec[2];

            if(arg1 == 0)
                pc = arg2;
            else
                pc += 3;
        }
        else if(op == 7) { // less than
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = 0;
            if(arg1 < arg2)
                program[out] = 1;

            pc += 4;
        }
        else if(op == 8) { // equals
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = 0;
            if(arg1 == arg2)
                program[out] = 1;

            pc += 4;
        }
        else if(op == 99) {
            break;
        }
    }

    return outputs;
}

int main() {
    ifstream in("input.txt");
    string line;
    getline(in, line);

    stringstream ss(line);
    vector<int> program;
    while(getline(ss, line, ',')) {
        program.push_back(stoi(line));
    }

    vector<int> outputs = runProgram(program, 1);
    cout << "part1: " << outputs.back() << endl;

    outputs = runProgram(program, 5);
    cout << "part2: " << outputs.back() << endl;

    in.close();
    return 0;
}