#include <bits/stdc++.h>

using namespace std;

int getIndex(int p, int offset, int len) {
    return min(len - 1, p + offset);
}

/* returns vector where:
 * first element is opcode
 * next element is VALUE of parameter 1
 * next element is VALUE of parameter 2 if any
 * final element is output address
 */
vector<int> decode(string instruction, int pc, vector<int> &program) {
    int len = instruction.length();
    int progLen = program.size();

    instruction = string(4 - len, '0') + instruction;
    len = instruction.length();

    bool imediateArg1 = (instruction.substr(len - 3, 1) != "0");
    bool imediateArg2 = (instruction.substr(len - 4, 1) != "0");
    vector<int> d(4);

    int off1 = getIndex(pc, 1, progLen);
    int off2 = getIndex(pc, 2, progLen);
    int off3 = getIndex(pc, 3, progLen);

    d[0] = stoi(instruction.substr(len - 2));

    if(d[0] == 1 || d[0] == 2 || d[0] == 7 || d[0] == 8) {
        d[1] = (imediateArg1) ? program[off1] : program[program[off1]];
        d[2] = (imediateArg2) ? program[off2] : program[program[off2]];
        d[3] = program[off3];
    }
    else if(d[0] == 3 || d[0] == 4) {
        d[1] = program[off1];
    }
    else if(d[0] == 5 || d[0] == 6) {
        d[1] = (imediateArg1) ? program[off1] : program[program[off1]];
        d[2] = (imediateArg2) ? program[off2] : program[program[off2]];
    }

    return d;
}

vector<int> runProgram(vector<int> program, vector<int> inputs) {
    int pc = 0;
    int currInput = 0;
    vector<int> outputs;

    while(pc < program.size()) {
        vector<int> dec = decode(to_string(program[pc]), pc, program);
        int op = dec[0];

        if(op == 1) { // add
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = arg1 + arg2;

            pc += 4;
        }
        else if(op == 2) { // mul
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = arg1 * arg2;

            pc += 4;
        }
        else if(op == 3) { // in
            int in = dec[1];
            program[in] = inputs[currInput];

            currInput++;
            pc += 2;
        }
        else if(op == 4) { // out
            int out = dec[1];
            outputs.push_back(program[out]);

            pc += 2;
        }
        else if(op == 5) { // jump-if-true
            int arg1 = dec[1];
            int arg2 = dec[2];

            if(arg1 != 0)
                pc = arg2;
            else
                pc += 3;
        }
        else if(op == 6) { // jump-if-false
            int arg1 = dec[1];
            int arg2 = dec[2];

            if(arg1 == 0)
                pc = arg2;
            else
                pc += 3;
        }
        else if(op == 7) { // less than
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = 0;
            if(arg1 < arg2)
                program[out] = 1;

            pc += 4;
        }
        else if(op == 8) { // equals
            int arg1 = dec[1];
            int arg2 = dec[2];
            int out = dec[3];

            program[out] = 0;
            if(arg1 == arg2)
                program[out] = 1;

            pc += 4;
        }
        else if(op == 99) {
            break;
        }
    }

    return outputs;
}

class Amplifier {
public:
    Amplifier(vector<int> program, int phase) : program(program), phase(phase) {
        inputs.push_back(phase);
    }
    vector<int> inputs;
    vector<int> outputs;
    bool halted = false;

    void execute() {
        runProgram();
    }
private:
    vector<int> program;
    int phase = 0;
    int pc = 0;
    int currInput = 0;

    void runProgram() {
        while(pc < program.size()) {
            vector<int> dec = decode(to_string(program[pc]), pc, program);
            int op = dec[0];

            if(op == 1) { // add
                int arg1 = dec[1];
                int arg2 = dec[2];
                int out = dec[3];

                program[out] = arg1 + arg2;

                pc += 4;
            }
            else if(op == 2) { // mul
                int arg1 = dec[1];
                int arg2 = dec[2];
                int out = dec[3];

                program[out] = arg1 * arg2;

                pc += 4;
            }
            else if(op == 3) { // in
                int in = dec[1];
                program[in] = inputs[currInput];

                currInput++;
                pc += 2;
            }
            else if(op == 4) { // out
                int out = dec[1];
                outputs.push_back(program[out]);

                pc += 2;
                break;
            }
            else if(op == 5) { // jump-if-true
                int arg1 = dec[1];
                int arg2 = dec[2];

                if(arg1 != 0)
                    pc = arg2;
                else
                    pc += 3;
            }
            else if(op == 6) { // jump-if-false
                int arg1 = dec[1];
                int arg2 = dec[2];

                if(arg1 == 0)
                    pc = arg2;
                else
                    pc += 3;
            }
            else if(op == 7) { // less than
                int arg1 = dec[1];
                int arg2 = dec[2];
                int out = dec[3];

                program[out] = 0;
                if(arg1 < arg2)
                    program[out] = 1;

                pc += 4;
            }
            else if(op == 8) { // equals
                int arg1 = dec[1];
                int arg2 = dec[2];
                int out = dec[3];

                program[out] = 0;
                if(arg1 == arg2)
                    program[out] = 1;

                pc += 4;
            }
            else if(op == 99) {
                halted = true;
                break;
            }
        }
    }
};


int solvePart1(vector<int> &program) {
    int result = INT_MIN;
    vector<int> settings({0, 1, 2, 3, 4});
    int in = 0;
    do {
        for(int s : settings) {
            vector<int> o = runProgram(program, vector<int>({s, in}));
            in = o.back();
        }
        result = max(result, in);
        in = 0;
    } while(next_permutation(settings.begin(), settings.end()));

    return result;
}

int runAmplifiers(vector<int> settings, vector<int> &program) {
    Amplifier a(program, settings[0]);
    Amplifier b(program, settings[1]);
    Amplifier c(program, settings[2]);
    Amplifier d(program, settings[3]);
    Amplifier e(program, settings[4]);

    bool first = true;
    int result = -1;
    bool br = false;

    while(!br) {
        a.inputs.push_back((first) ? 0 : e.outputs.back());
        if(!a.halted) {
            a.execute();
            br = (br == false) ? a.halted : br;
            first = false;
        }

        b.inputs.push_back(a.outputs.back());
        if(!b.halted) {
            b.execute();
            br = (br == false) ? b.halted : br;
        }

        c.inputs.push_back(b.outputs.back());
        if(!c.halted) {
            c.execute();
            br = (br == false) ? c.halted : br;
        }

        d.inputs.push_back(c.outputs.back());
        if(!d.halted) {
            d.execute();
            br = (br == false) ? d.halted : br;
        }

        e.inputs.push_back(d.outputs.back());
        if(!e.halted) {
            e.execute();
            br = (br == false) ? e.halted : br;
        }

        result = e.outputs.back();
    }

    return result;
}

int solvePart2(vector<int> &program) {
    int result = INT_MIN;
    vector<int> settings({5, 6, 7, 8, 9});
    do {
        int o = runAmplifiers(settings, program);
        result = max(result, o);
    } while(next_permutation(settings.begin(), settings.end()));

    return result;
}

int main() {
    ifstream in("input.txt");
    string line;
    getline(in, line);

    stringstream ss(line);
    vector<int> program;
    while(getline(ss, line, ',')) {
        program.push_back(stoi(line));
    }

    cout << "part1: " << solvePart1(program) << endl;
    cout << "part2: " << solvePart2(program) << endl;

    in.close();
    return 0;
}