#include <bits/stdc++.h>

using namespace std;

bool validPart1(int p) {
    bool hasDuplicate = false;

    int lastDigit = p % 10;
    p /= 10;
    while(p > 0) {
        int currDigit = p % 10;
        if(currDigit > lastDigit)
            return false;

        if(currDigit == lastDigit)
            hasDuplicate = true;

        lastDigit = currDigit;
        p /= 10;
    }
    return hasDuplicate;
}

int solvePart1(int l, int r) {
    int result = 0;
    for(int i = l; i <= r; i++) {
        if(validPart1(i))
            result++;
    }
    return result;
}

bool validPart2(int p) {
    bool hasDuplicate = false;
    int count = 0;
    int lastCount = 0;
    int lastDigit = -1;
    while(p > 0) {
        int currDigit = p % 10;

        if(currDigit > lastDigit && lastDigit != -1)
            return false;

        if(currDigit == lastDigit) {
            count = (count == 0) ? 2 : count+1;
        }
        else {
            lastCount = count;
            count = 0;
            if(!hasDuplicate)
                hasDuplicate = (lastCount == 2);
        }

        lastDigit = currDigit;
        p /= 10;
    }

    return (count == 2) || hasDuplicate;
}

int solvePart2(int l, int r) {
    int result = 0;
    for(int i = l; i <= r; i++) {
        if(validPart2(i))
            result++;
    }
    return result;
}

int main() {
    ifstream in("input.txt");
    string line;
    vector<int> range;

    while(getline(in, line, '-')) {
        range.push_back(stoi(line));
    }

    cout << "part1: " << solvePart1(range[0], range[1]) << endl;
    cout << "part2: " << solvePart2(range[0] , range[1]) << endl;

    in.close();
    return 0;
}