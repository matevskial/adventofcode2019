#include <bits/stdc++.h>

using namespace std;

int getSum(int m) {
    int sum = 0;
    while(m > 6) {
        int f = (m / 3 - 2);
        sum += f;
        m = f;
    }
    return sum;
}

int main() {
    fstream in("input.txt");
    string line;

    int sumPart1 = 0;
    int sumPart2 = 0;
    while(getline(in, line)) {
        int module = stoi(line);
        sumPart1 += (module / 3 - 2);
        sumPart2 += getSum(module);
    }

    cout << "part1: " << sumPart1 << endl;
    cout << "part2: " << sumPart2 << endl;

    in.close();
}