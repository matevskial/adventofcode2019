# Day 1
Run:

```bash
g++ day01.cpp -o day01
./day01
part1: 3380731
part2: 5068210
```
